<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    const NO_OF_USERS = 3;
    const NO_OF_POSTS = 50;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, self::NO_OF_USERS)->create();
        $this->command->info('Users created!');

        factory(App\Post::class, self::NO_OF_POSTS)->create();
        $this->command->info('Posts Created!');
    }
}
