# Installation

Requirements:

1. PHP ^ 7.1
2. VirtualBox or another provider
3. Vagrant (i think v2.0.2 ^) with box laravel/homestead

Installing steps:

copy .env.example in .env
composer install
php vendor/bin/homestead make

In generated homestead.yaml file, make the following changes:
1. add line  --> elasticsearch: 6
2. change database name in 'test' (in order to have the same name as in env file)

vagrant up

vagrant ssh

cd /vagrant

php artisan key:generate

php artisan migrate

php artisan db:seed

add the following line in /etc/hosts --> 192.168.10.10 neurony-test.local

access app at http://neurony-test.local in browser